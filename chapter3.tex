\chapter{Algoritmi su grafi con applicazioni su reti biologiche e sociali}
Graphtool permette di eseguire una serie di algoritmi sui grafi fornendo all'utente una chiara ed
intuitiva interfaccia che lo guida nella selezione e nell'esecuzione dell'algoritmo desiderato. I risultati
sono mostrati in un apposito log e sono agevolmente consultabili. Inoltre, a seconda dell'algoritmo scelto e
del risultato ottenuto, la rete sui cui è eseguito assume connotazioni visive tali da descrivere visivamente
il risultato di tale algoritmo. 

Graphtool consente, inoltre, di assegnare valori reali per i pesi degli archi dei grafi. È possibile pertanto
poter calcolare i cammini minimi (Algoritmo di Dijkstra) a partire da un vertice sorgente ad un
vertice destinazione prefissati. Si può scegliere di calcolare tali cammini considerando il grafo come
pesato o non pesato (in questo ultimo caso il peso di un cammino è dato dal numero di archi da cui è composto).

Segue, in questo capitolo, la descrizione dell'algoritmo di Dijkstra e delle misure di Centralità supportate da Graphtool.

\section{Dijkstra Shortest Path}\index{Dijkstra Shortest Path}
Si supponga di considerare un grafo \begin{math}G=(V,E)\end{math} i cui pesi degli archi siano tutti
non negativi, ossia valga \begin{math}w(u,v)\ge 0\end{math} per ogni arco \begin{math}(u,v)\in E\end{math}.
Si supponga, dunque, di voler calcolare il camminino minimo a partire da un vertice sorgente singolo
prefissato, che indicheremo con \begin{math}s\end{math}. Per un vertice \begin{math}u\end{math} indichiamo
con \begin{math}d[u]\end{math} la stima del costo del cammino minimo da \begin{math}s\end{math} ad
\begin{math}u\end{math} correntemente aggiornata. Indichiamo, inoltre, il predecessore di \begin{math}u\end{math}
con \begin{math}\pi [u]\end{math}.

L'algoritmo di Dijkstra costruisce un insieme, che indicheremo con \begin{math}S\end{math}, contenente
tutti i vertici il cui peso di cammino minimo dalla sorgente è stato determinato, ossia tutti i vertici
\begin{math}v \in S\end{math} per cui \begin{math}d[v] = \delta (s,v)\end{math}. L'algoritmo seleziona, ad ogni passo,
il vertice \begin{math}u \in V-s\end{math} avente la stima di cammino minimo,
inserisce \begin{math}u\end{math} in \begin{math}S\end{math} ed esegue l'operazione di rilassamento di
tutti gli archi uscenti \begin{math}u\end{math} \cite{cormen-leiserson}. Tale operazione, per
un generico arco \begin{math}(u,v)\end{math} consiste nel valutare se è possibile migliorare il cammino
minimo per \begin{math}v\end{math} trovato fino a quel momento con un nuovo cammino passante per
\begin{math}u\end{math}. In un secondo momento aggiorna la stima di cammino minimo ed imposta, eventualmente,
il nuovo predecessore di \begin{math}v\end{math}. In Algoritmo \ref{alg:relax} è mostrata la procedura di
rilassamento di un arco, mentre in Algoritmo \ref{alg:dijkstra} è mostrato l'algoritmo per la ricerca dei
cammini minimi.

\begin{algorithm}{RELAX(u,v,w)}\index{Relax}
\caption{Rilassamento di un arco}
\label{alg:relax}
\begin{algorithmic}[1]
\IF{$d[v] > d[u]+w(u,w)$}
\STATE $d[v] \leftarrow d[u]+w(u,v)$
\STATE $\pi[v] \leftarrow u$
\ENDIF
\end{algorithmic}
\end{algorithm}

\begin{algorithm}{DIKJKSTRA(G,w,s)}
\caption{Algoritmo di Dijkstra}
\label{alg:dijkstra}
\begin{algorithmic}[1]
\STATE{Per ogni vertice $v \in V$ imposta $d[v] \leftarrow \infty$ e $\pi[v] \leftarrow $NIL}
\STATE $d[s] \leftarrow 0$
\STATE $S \leftarrow \varnothing$
\STATE $Q \leftarrow V$
\WHILE{$Q \neq \varnothing$}
\STATE {$u \leftarrow$ elemento con valore minimo da $Q$}
\STATE $S \leftarrow S \cup \{u\}$
\FOR{ogni vertice $v \in V$}
\STATE {RELAX($u,v,w$)}
\ENDFOR
\ENDWHILE
\end{algorithmic}
\end{algorithm}

\section{Misure di Centralità}\index{Centrality}
Come già ampiamente anticipato, dato un grafo è possibile definire una misura della centralità
dei nodi (ma anche degli archi) che la costituiscono. Una misura della centralità, intuitivamente,
dà informazioni su quanto un nodo della rete sia più \textit{centrale}, ossia più \textit{importante},
rispetto ad un altro \cite{koschutzki,network-analysis,wassernman}. Esistono molteplici
misure di centralità e Graphtool ne supporta svariate: un nodo quindi, può risultare più o meno \textit{centrale}
a seconda della misura di centralità adottata.

\subsection{Degree Centrality}\index{Degree Centrality}
La Degree Centrality è una misura di centralità basata sul grado (entrante o uscente) dei
nodi di un grafo. Sia \begin{math}G=(V,E)\end{math} un
grafo con \begin{math}n\end{math} vertici e sia \begin{math}deg(v)\end{math} il grado del vertice
\begin{math}v\end{math}. Allora la Degree Centrality \begin{math}C_D(v)\end{math} è definita come
\begin{equation}
C_D(v)=\frac{\text{deg}(v)}{n-1}
\end{equation}

A seconda che si considerino solo archi entranti od uscenti si potrà definire rispettivamente la InDegree e OutDegree Centrality.
Si noti come questa misura di centralità dia solo una misurazione locale in quanto, per ciascun nodo è definita solo in
funzione dei nodi immediatamente adiacenti.

La Degree Centrality può essere di grande utilità quando si analizza una rete sociale in cui gli archi rappresentano relazioni
sociali tra individui. Ad esempio, si supponga di considerare una rete sociale i cui nodi rappresentano studenti di un
corso di laurea che vogliono eleggere il loro rappresentante e gli archi rappresentano \emph{chi ha votato chi} (\figurename~\ref{votazione}).
Appare evidente in questo contesto che uno studente è più importante, e quindi più centrale di un
altro, se avrà ricevuto un maggior numero di voti (determinati dal numero di archi entranti per nodo).
In questo caso la misura di centralità che mette in evidenza questa importanza è proprio la Indegree Centrality.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=8cm,keepaspectratio]{figures/votazione.eps}
\caption[Esempio di Indegree Centrality]{Esempio di Indegree Centrality: gli studenti A e B di un corso di laurea
votano entrambi per lo studente C quale loro rappresentante. In questo caso lo studente C è il più centrale.}\label{votazione}
\end{figure}

\subsection{Node/Edge Betweenness}\index{Node Betwenness}
Sia
\begin{displaymath}
\delta_{st}(v)=\frac{\sigma_{st}(v)}{\sigma_{st}}
\end{displaymath}
dove \begin{math}\sigma_{st}\end{math} è il numero di cammini minimi tra il nodo \begin{math}s\end{math}
e \begin{math}t\end{math} mentre \begin{math}\sigma_{st}(v)\end{math} rappresenta il numero di cammini minimi tra il nodo \begin{math}s\end{math}
e \begin{math}t\end{math} passanti per $v$. Possiamo asserire che \begin{math}\delta_{st}\end{math} rappresenti la
probabilità che il nodo \begin{math}v\end{math} sia coinvolto in una \emph{comunicazione} tra \begin{math}s\end{math}
e \begin{math}t\end{math}. Allora la misura di centralità detta Betweenness è definita come
\begin{equation}
C_{B}(v)=\sum_{s\ne v\in V}\sum_{t\ne v\in V}\delta_{st}(v)
\end{equation}

Analogamente alla Betweenees dei nodi di un grafo è possibile fornire un'analoga misura di centralità in funzione degli archi.

La Betweenness Centrality può essere utile, nell'ambito delle reti sociali, per la misurazione del potere di controllo del flusso
della conoscenza. In tale contesto, generalmente, i nodi con un'alta Betweenness Centrality sono definiti \textit{brokers}. Un
broker può essere un membro di una rete sociale tramite il quale avviene la comunicazione tra gli altri nodi (in questo caso è
detto \textit{coordinator}), un membro che ottiene dall'esterno il flusso di conoscenza e lo trasferisce ad un gruppo
(\textit{gatekeeper}), un membro che trasferisce la conoscenza tra membri dello stesso gruppo a cui esso tuttavia, non appartiene
(\textit{itinerant}) o, infine, un membro che trasferisce la conoscenza tra distinti gruppi della rete sociale (\textit{liason}). In
tutti questi casi il broker ha una funzione importante all'interno della rete sociale. Tale importanza viene messa in evidenza
da valori di Betweenness Centrality più alti.

\subsection{Closeness}\index{Closeness}
Sia \begin{math}\sum_{v\in V}d(u,v)\end{math} la somma delle distanze da un vertice
\begin{math}u \in V\end{math} ad ogni altro. Allora la Closeness Centrality è definita come
\begin{equation}
C_D(u)=\frac{1}{\sum_{v\in V}d(u,v)}
\end{equation}
Tale funzione cresce se la distanza totale di \begin{math}u\end{math} decresce. In \figurename~\ref{closeness} è
mostrato un esempio. La Closeness Centrality può risultare molto utile quando si vuole determinare il nodo che minimizza
la distanza totale da tutti gli altri. Un esempio pratico consiste nel ricercare la posizione ottimale di un centro
commerciale in una città (rappresentata adeguatamente da un grafo). 
Una buona posizione potrebbe essere quella che minimizza la distanza del centro commerciale
da tutti i clienti in modo da ottimizzare lo spostamento della maggior parte dei clienti.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=8cm,keepaspectratio]{figures/closeness.eps}
\caption[Esempio di Closeness Centrality]{Esempio di Closeness Centrality: in questo caso i valori indicati
rappresentano le distanze totali. In questo caso i nodi \begin{math}u\end{math} e \begin{math}v\end{math}
sono i più centrali.}\label{closeness}
\end{figure}

\subsection{Barycenter}\index{Barycenter}
Sia \begin{math}p \in V\end{math}, un vertice di un grafo. Allora la Baricenter Centrality è definita come
\begin{equation}
U_{center}(p)=\sum_{(u,v)\in E}{\|p_u-p_v\|^2}
\end{equation}
Con questa misura di centralità viene assegnata una importanza ad un vertice in funzione della somma delle
distanze del nodo da tutti gli altri \cite{brandes}.

\subsection{Page Rank}\index{Page Rank}
L'algoritmo Page Rank \cite{brin-anatomy} è ampiamente utilizzato nel World Wide Web nell'ambito
della classificazione delle pagine web in relazione alla loro importanza: essa è tipicamente determinata
dalla quantità di collegamenti ipertestuali presenti nella pagina e a quanto importanti sono le pagine web linkate.
In questo caso le pagine web rappresentano i nodi di un grafo mentre i link tra le pagine rappresentano gli archi.
Tale algoritmo è utilizzato su larga scala da alcuni tra i principali motori di ricerca disponibili sul web.
Si supponga di voler calcolare l'algoritmo di Page Rank su un nodo \begin{math}p\end{math} (una pagina web).
Indichiamo con \begin{math}P_{p}\end{math} l'insieme dei nodi raggiungibili a partire da \begin{math}p\end{math} e sia
\begin{math}d\end{math} un fattore di smorzamento (dumping factory). Allora la misura di centralità indotta da Page Rank per
un nodo \begin{math}p\end{math} è definita come
\begin{equation}
C_{PR}(p)=d\sum_{q\in P_p^{-}}\frac{C_{PR}(q)}{d^{+}q}+(1-d)
\end{equation}
La misura della centralità basata su Page Rank si inserisce nel generico di concetto di Web Centralities,
ossia di centralità orientate al Web. Anche le misure di centralità presentate di seguito (Hub e Authority) appartengono
a tale categoria.

\subsection{Authority \& Hub}\index{Hub}\index{Authority}
Nell'ambito del World Wide Web possiamo definire come Authority una pagina web \textit{autorevole},
ossia una pagina web che contiene informazioni rilevanti e precise sull'argomento desiderato e, quindi, linkata da
parecchie altre pagine web. Possiamo altresì definire come Hub, una pagina web contenente molti collegamenti verso
pagine web \textit{autorevoli}, ossia verso Authorities. Come descritto da Kleinberg \cite{kleinberg-1,kleinberg-2}
un buon \textit{hub} è, dunque, una pagina web che punta a molte buone \textit{autorithies} e, viceversa,
una buona \textit{autorithy} è una pagina web che punta a molti
buoni \textit{hub}: appare dunque evidente che i due concetti sono strettamente correlati l'uno con l'altro.
L'algoritmo per il calcolo dell'hub e dell'autority è stato proposto da Kleinberg e consta di due fasi: la
prima fase consiste nella ricerca delle pagine web, a partire da una query definita dall'utente: questa fase,
nel nostro caso, non interessa in quanto vogliamo effettuare il calcolo su una network prestabilita e già
caricata nello spazio di lavoro di Graphtool. La seconda fase consiste nel vero e proprio calcolo dei valori
hub e autorithy. Sia, dunque, \begin{math}G\end{math} un grafo e siano \begin{math}c_{HA-H} = Ac_{HA-A}\end{math}
con \begin{math}c_{HA-A}\end{math} noto e \begin{math}c_{HA-A} = Ac_{HA-H}\end{math} con \begin{math}c_{HA-H}\end{math}
noto, rispettivamente i valori di hub ed autority, dove \begin{math}A\end{math} è la matrice d'adiacenza del
grafo \begin{math}G\end{math}: l'algoritmo per il calcolo di tali valori restituisce le approssimazioni
di \begin{math}c_{HA-H}\end{math} e \begin{math}c_{HA-A}\end{math}. Si veda a tal proposito \ref{alg:hub-authority} per
un'implementazione dell'algoritmo per il calcolo delle approssimazioni dell'hub e dell'authority.

\begin{algorithm}
\caption{Calcolo dell'Hub e dell'Authority}
\label{alg:hub-authority}
\begin{algorithmic}[1]
\ENSURE: approssimazioni di $c_{HA-A}$ e $c_{HA-H}$
\STATE $c_{HA-A}^0 \leftarrow 1_{n}$
\FOR {$k=1 \ldots$}
\STATE $c_{HA-H}^{k} \leftarrow Ac_{HA-A}^{k-1}$
\STATE $c_{HA-A}^{k} \leftarrow Ac_{HA-C}^{k}$
\STATE $c_{HA-H}^{k} \leftarrow \frac{c_{HA-H}^k}{\|c_{HA-H}^k\|}$
\STATE $c_{HA-A}^{k} \leftarrow \frac{c_{HA-A}^k}{\|c_{HA-A}^k\|}$
\ENDFOR
\end{algorithmic}
\end{algorithm}

