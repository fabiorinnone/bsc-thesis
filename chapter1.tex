\chapter{Panoramica di Graphtool}
\lhead[\fancyplain{}{\bfseries\thepage}]{\fancyplain{}{\bfseries\rightmark}}
\rhead[\fancyplain{}{\bfseries\leftmark}]{\fancyplain{}{\bfseries\thepage}}
\pagenumbering{arabic}

In questo capitolo presenteremo una panoramica generale di Graphtool, mostrandone dettagli relativi all'interfaccia utente. 
La parte di codice relativa ad analisi e visualizzazione di Graphtool si basa sulla libreria Java Universal Network/Graph Framework 
(JUNG2.0)\index{JUNG} \cite{madadhain}. Essa permette di modellare, analizzare e visualizzare grafi. Scritta in Java, 
JUNG2.0 si presta perfettamente per la realizzazione di Graphtool. Per maggiori dettagli implementativi si rimanda il lettore all'appendice.

\section{Definizioni preliminari}
Graphtool è stato sviluppato per la visualizzazione ed analisi di grafi con particolare riferimento a reti biologiche e sociali. 
Presenteremo, quindi, alcune definizioni preliminari riguardanti i grafi.

\begin{defin} 
Un grafo\index{Grafo} è una coppia \begin{math}G = (V,E)\end{math} dove \begin{math}V\end{math} è l'insieme dei vertici 
(o nodi) ed \begin{math}E \end{math} è l'insieme degli archi \begin{math}(v,w)\end{math} tali che \begin{math}v \in V\end{math}
e \begin{math}w \in V\end{math}. Il numero di vertici e di archi è dato rispettivamente da \begin{math}|V|\end{math} e
\begin{math}|E|\end{math}.
\end{defin}

\begin{defin} 
Un grafo si dice orientato quando $E$ è un insieme di coppie ordinate. Un arco \begin{math}(w,u)\end{math} si dice 
\textit{incidente}\index{Arco incidente} da \begin{math}w\end{math} in \begin{math}u\end{math}.
\end{defin}

\begin{defin} 
Un grafo si dice non orientato quando $E$ è un insieme di coppie non ordinate.
\end{defin}

\begin{defin}\index{Matrice d'adiacenza}
Una matrice d'adiacenza \begin{math}A(a_{ij})\end{math} di un grafo è una matrice \begin{math}\mid V\mid \times \mid V\mid \end{math}
tale che
\begin{equation}
a_{ij} = \left\{
\begin{array}{rl}
1 & \text{se } (i,j) \in E,\\
0 & \text{altrimenti}.
\end{array} \right.
\end{equation}
\end{defin}
In \figurename~\ref{graph-matrix} è mostrato un semplice grafo e la sua matrice d'adiacenza.

\begin{defin} Il grado (degree) di un vertice \begin{math}u \in V\end{math} di un grafo non orientato
è il numero di archi \begin{math}(u,w) \in E\end{math}.\end{defin}\index{Degree}

\begin{defin} Il grado entrante (indegree) di un vertice \begin{math}w \in V\end{math} di un grafo orientato
è il numero di archi \begin{math}(u,w) \in E\end{math} incidenti in esso.\end{defin}\index{Indegree}

\begin{defin} Il grado uscente (outdegree) di un vertice \begin{math}u \in V\end{math} di un grafo orientato
è il numero di archi \begin{math}(u,w) \in E\end{math} che da esso si dipartono.\end{defin}\index{Outdegree}

\begin{defin} Dato un grafo è possibile definire una funzione peso (\textit{weight})
\begin{math}w: E\rightarrow \mathbb{R}\end{math} tale che associ ad ogni arco un valore nell'insieme
dei numeri reali.\end{defin}\index{Peso}

\begin{defin} Dato un grafo pesato, il peso di un cammino \begin{math}p\end{math} da un vertice \begin{math}v_0\end{math} 
ad un vertice \begin{math}v_k\end{math} (tali che \begin{math}v_i \in V\end{math} con \begin{math}i=0,1,...,k\end{math})
è definito da
\begin{equation}
w(p)=\sum_{i=1}^{k}w(v_{i-1},v_i)
\end{equation}
\end{defin}

\begin{figure}[htbp]
\centering%
\linespread{1.0}
\subfigure[\label{graph}]%
{\includegraphics[width=5cm]{figures/graph.eps}}\qquad\qquad
\subfigure[\label{matrix}]%
{\includegraphics[width=4cm]{figures/matrix.eps}}
\caption[Grafo e matrice d'adiacenza]{Grafo e matrice d'adiacenza\label{graph-matrix}. (a) Grafo orientato \begin{math}G\end{math}. 
(b) Matrice d'adiacenza di \begin{math}G\end{math}.}
\end{figure}

Graphtool fornisce un'intuitiva interfaccia utente utile per la rappresentazione dei grafi e l'applicazione di diversi algoritmi. 
Segue adesso una rapida panoramica dell'interfaccia utente.

\section{Interfaccia utente}
All'avvio dell'applicazione viene mostrato un framework con un pannello laterale ed un pannello centrale principale.
In \figurename~\ref{schermata} è mostrata la schermata principale di Graphtool con all'interno la rappresentazione di un grafo.

Il pannello laterale mostra le opzioni principali mentre il pannello centrale permette di visualizzare contemporaneamente 
diversi grafi (gestiti tutti in  memoria principale). L'utente puo decidere se creare un nuovo grafo, aprirne uno esistente, 
salvare, chiudere i grafi correntemente aperti o uscire dall'applicazione. L'utente puo anche scegliere se visualizzare o 
nascondere il pannello laterale delle opzioni in modo da ottenere piu spazio per l'editing o la visualizzazione 
dei grafi aperti. Dal pannello laterale è possibile visualizzare la lista dei grafi correntemente caricati, le opzioni di 
visualizzazione ed impostare le opzioni per l'esecuzione di vari algoritmi.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=12cm,keepaspectratio]{figures/graphtool_schermata.eps}
\caption[Schermata principale di Graphtool]{Schermata principale di Graphtool.}\label{schermata}
\end{figure}

\begin{figure}[htbp]
\centering%
\linespread{1.0}
\subfigure[\label{pannello_grafi}]%
{\includegraphics[width=4cm]{figures/pannello_grafi.eps}}\qquad\qquad
\subfigure[\label{pannello_opzioni}]%
{\includegraphics[width=4cm]{figures/pannello_opzioni.eps}}\qquad\qquad
\subfigure[\label{pannello_algoritmi}]%
{\includegraphics[width=4cm]{figures/pannello_algoritmi.eps}}\qquad\qquad
\subfigure[\label{pannello_netmatch}]%
{\includegraphics[width=4cm]{figures/pannello_netmatch.eps}}
\caption[Pannello laterale di Graphtool]{Pannello laterale di Graphtool\label{pannello_laterale}. (a) Visualizzazione 
dei grafi correntemente caricati con rispettivi numero di vertici e di archi. (b) Opzioni di visualizzazione. (c) 
Scelta e calcolo delle misure di centralità su grafi. (d) Ricerca di sottostrutture in un grafo.}
\end{figure}

\section{Creazione e caricamento di grafi}
\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=12cm,keepaspectratio]{figures/layout_circolare.eps}
\caption[Grafo con layout circolare]{Grafo con layout circolare: layout predefinito di visualizzazione dei grafi.}\label{schermata}
\end{figure}

Alla creazione di un nuovo grafo viene aperta una nuova scheda in corrispondenza del pannello principale che permette 
di disegnare un nuovo grafo. Utilizzando il mouse l'utente puo creare vertici ed archi e visualizzare proprietà del 
grafo corrente, editare etichette e attributi dei vertici e degli archi o modificarne proprietà grafiche (quali spessore 
della linea di contorno o colore). È data anche la possibilita di decidere se visualizzare o meno i grafi caricati. 
Quest'ultima opzione risulta particolarmente utile quando si ha a che fare con grafi di grandi dimensioni per i quali 
la visualizzazione potrebbe non essere possibile a causa delle enormi richieste in termini di spazio di memoria. Con 
tale opzione è possibile caricare comunque un grafo, anche se non visualizzato, al fine di poter applicare comunque 
un algoritmo su di esso. Per evitare un sovraccarico della memoria i grafi caricati da file con un numero di vertici 
superiore a 1000 non sono visualizzati come impostazione di default.

\section{Opzioni di visualizzazione}
Dal pannello delle opzioni è possibile settare alcune impostazioni di visualizzazione dei grafi. In particolare l'utente 
puo scegliere la modalità di interazione con i grafi correntemente aperti: \texttt{Editing} permette di aggiungere o 
rimuovere vertici ed archi, \texttt{Transforming} permette di ruotare o ridimensionare il grafo, \texttt{Piking} permette 
di spostare vertici ed archi. Possono inoltre essere impostate le viste iperboliche ossia particolari metodi di visualizzazione 
pseudo tridimensionale (\figurename~\ref{vista_iperbolica}) e selezionati diversi layout di disposizione dei grafi sul piano. 
Il layout predefinito è quello circolare: in tal caso i vertici vengono disposti in eguale distanza dal centro del pannello
di visualizzazione e in eguale distanza gli uni dagli altri. Altre opzioni di visualizzazione riguardano la tipologia di archi 
(ad esempio curvilinei od ortogonali), il tipo di colorazione degli archi, la posizione e la visualizzazione delle 
etichette sia dei vertici che degli archi.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=12cm,keepaspectratio]{figures/vista_iperbolica.eps}
\caption[Hyperbolic View]{Hyperbolic View.}\label{vista_iperbolica}
\end{figure}


