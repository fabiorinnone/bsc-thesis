# Makefile per tesi in LaTeX

default: all

all:
	latex frontispiece.tex
	latex thesis.tex
	makeindex -s index.ist thesis.idx
	bibtex thesis
	latex thesis.tex
	latex thesis.tex
	dvips -t a4 frontispiece.dvi -o
	dvips -t a4 thesis.dvi -o
	ps2pdf frontispiece.ps
	ps2pdf thesis.ps
		
.tex:
	latex $@
	makeindex -s index.ist $@.tex
	bibtex $?.bib
	latex $@
	latex $@
	dvips -t a4 $?.dvi -o
	ps2pdf $@.ps
	
clean:
	rm -f *.aux*
	rm -f *.log*
	rm -f *.toc*
	rm -f *.loa*
	rm -f *.lot*
	rm -f *.lof*
	rm -f *.idx*
	rm -f *.bbl*
	rm -f *.blg*
	rm -f *.idx*
	rm -f *.ilg*
	rm -f *.ind*
	rm -f *.bib?
	rm -f *.tex?
	rm -f *.ps*
	rm -f Makefile?
	rm -f README?

erase:
	make clean
	rm -f *.dvi*
	rm -f *.ps*
	rm -f *.pdf*

help:
	@echo "make [all]: genera i file .dvi e .pdf di tutti i file .tex"
	@echo "make frontispiece: genera i file .dvi e .pdf del frontespizio"
	@echo "make theis: genera i file .dvi e .pdf della tesi" 
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e i file .dvi, .ps e .pdf"
	@echo "Leggere anche README"
