\chapter{Subgraph matching}\index{Subgraph Matching}
In questo capitolo verrà presentata un'altra funzionalità di Graphtool che permette di ricercare tutte le occorrenze di
sottostrutture \begin{math}G_1\end{math} (indicato anche come grafo query) all'interno di un grafo \begin{math}G_2\end{math}
(indicato anche come grafo target) (\figurename~\ref{query-target}). Questo problema noto come subgraph matching è un
problema NP-Completo \cite{garey} per cui non esistono soluzioni polinomiali. Nonostante ciò, esistono diversi algoritmi
che risultano particolarmente efficienti anche su grandi grafi. Il metodo implementato su Graphtool si basa sull'algoritmo
VF2 \cite{cordella}.

\begin{figure}[htbp]
\centering%
\linespread{1.0}
\subfigure[\label{query_graph}]%
{\includegraphics[width=4cm]{figures/query_graph.eps}}\qquad\qquad
\subfigure[\label{target_graph}]%
{\includegraphics[width=5.5cm]{figures/target_graph.eps}}
\caption[Subgraph Matching]{Subgraph Matching\label{query-target}. (a) Grafo query. (b) Grafo target.}
\end{figure}

\section{Algoritmo di ricerca}\index{VF2 Algorithm}
L'algoritmo di ricerca implementato in Graphtool è descritto in \cite{cordella} e fornisce una soluzione
più efficiente rispetto ad altri algoritmi per la ricerca delle sottostrutture in grafi \cite{ullman}. L'algoritmo
è applicabile ad ogni tipo di grafo, è particolarmente adatto a grafi con etichette o attributi, utilizza una
rappresentazione nello spazio degli stati con una strategia di ricerca depth-first (in profondità) ed ha cinque
regole di fattibilità (\textit{feasibility rules}) che permettono di ridurre lo spazio degli stati.

Il matching tra due grafi \begin{math}G_1=(V_1,E_1)\end{math} e \begin{math}G_2=(V_2,E_2)\end{math} consiste nel determinare
una mappatura \begin{math}M\end{math} che associa nodi di \begin{math}G_1\end{math} a nodi di \begin{math}G_2\end{math} e
viceversa, tenendo conto di alcune costanti predefinite. Una mappatura \begin{math}M\end{math} è un insieme di coppie
\begin{math}(n,m)\end{math} con \begin{math}n \in G_1\end{math} e \begin{math}m \in G_2\end{math}. Definiamo, inoltre, 
l'insieme \begin{math}M(s)\end{math}, come un sottoinsieme di \begin{math}M\end{math} che identifica univocamente
due sottografi di \begin{math}G_1\end{math} e \begin{math}G_2\end{math}, rispettivamente indicati come \begin{math}G_1(s)\end{math}
e \begin{math}G_2(s)\end{math} tale da rappresentare il matching parziale dello stato \begin{math}s\end{math} ossia l'insieme
corrente delle coppie di nodi dei due grafi che sono corrispondenti.

Una transizione da uno stato \begin{math}s\end{math} ad uno stato successore \begin{math}s'\end{math} consiste
nell'aggiunta nello spazio degli stati di una coppia di nodi \begin{math}(n,m)\end{math}. Otteremo, dunque, un nuovo insieme
\begin{equation}
M(s')=M(s)\cup(n,m)\text{ con }n\in V_1\text{ e }m\in V_2
\end{equation}

Per stabilire se una coppia \begin{math}(n,m)\end{math} da aggiungere allo stato \begin{math}s\end{math} produce uno stato inconsistente
o meno si utilizza un'apposita funzione di fattibilità \begin{math}F(s,n,m)\end{math} che ha lo scopo di filtrare più \textit{strade} possibili
per cercare di arrivare più velocemente alla soluzione. A tal scopo si costruisce un insieme \begin{math}P(s)\end{math} delle coppie
candidate. La scelta delle coppie da inserire in \begin{math}P(s)\end{math} sfrutta i cosidetti \textit{terminal sets}, indicati
rispettivamente con \begin{math}T_1\end{math} e \begin{math}T_2\end{math}, che contengono i nodi direttamente connessi a quelli già
inseriti nel mapping parziale e che provengono da \begin{math}V_1\end{math} e \begin{math}V_2\end{math}. Le coppie vengono scelte
in modo da assicurare che l'eventuale inconsistenza venga riconosciuta quanto più velocemente possibile.

L'algoritmo VF2 è descritto in Algoritmo \ref{vf2-pseudocode}, mentre in \figurename~\ref{vf2_algorithm} sono schematizzati i suoi passi
fondamentali.

\begin{algorithm}{MATCH(s)}
\caption{Algoritmo di matching VF2}
\label{vf2-pseudocode}
\begin{algorithmic}[1]
\REQUIRE{uno stato intermedio $s$. Lo stato iniziale è $s_0$ ha $M(s_0) = \varnothing$}
\ENSURE{la mappatura tra due grafi}
\IF{$M(s)$ copre tutti i nodi di $G_2$}
\STATE {mostra $M(S)$}
\ELSE
\STATE{calcola l'insieme $P(s)$ delle coppie candidate per l'inclusione in $M(s)$}
\FORALL{$p \in M(s)$}
\IF{$p$ è una fattibile coppia}
\STATE{calcola lo stato $s'$ ottenuto aggiungendo $p$ a $M(s)$}
\STATE{MATCH($s'$)}
\ENDIF
\ENDFOR
\STATE{aggiorna le strutture dati}
\ENDIF
\end{algorithmic}
\end{algorithm}

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=14cm,keepaspectratio]{figures/vf2_algorithm.eps}
\caption[Passi dell'algoritmo VF2]{Passi dell'algoritmo VF2.}\label{vf2_algorithm}
\end{figure}

\section{Opzioni di ricerca}\index{Query esatta}\index{Query approssimata}
L'agortimo VF2 è stato adattato in Graphtool in modo da supportare ricerche \textit{esatte} e \textit{approssimate}. 
Nel primo caso è data all'utente la possibilità di impostare le etichette dei vertici e degli archi della query (\figurename~\ref{query_esatta}),
nel secondo caso (impostazione predefinita nel caso in cui l'utente disegna il grafo query) è data la possibilità
di impostare vertici ed archi non etichettati (\figurename~\ref{query_approssimata}) che in questo caso possono essere accoppiati a qualunque nodo
del grafo target. Sono inoltre supportate le ricerche con nodi con \textit{self-loops} (\figurename~\ref{self-loop}) e con archi multipli (\figurename~\ref{multiple-edges}).

\begin{figure}[htbp]
\centering%
\linespread{1.0}
\subfigure[\label{query_esatta}]%
{\includegraphics[width=4cm]{figures/query_esatta.eps}}\qquad\qquad
\subfigure[\label{query_approssimata}]%
{\includegraphics[width=4cm]{figures/query_approssimata.eps}}
\caption[Tipi di Query]{Tipi di Query\label{tipi_query}. (a) Query esatta. (b) Query approssimata.}
\end{figure}

\begin{figure}[htbp]
\centering%
\linespread{1.0}
\subfigure[\label{self-loop}]%
{\includegraphics[width=2.5cm]{figures/self_loop.eps}}\qquad\qquad
\subfigure[\label{multiple-edges}]%
{\includegraphics[width=6.5cm]{figures/multiple_edges.eps}}
\caption[Caratteristiche delle query]{Caratteristiche delle query\label{edges}. (a) Self-loop. (b) Archi multipli (\textit{multiple edges}).}
\end{figure}

Una vota selezionati il grafo query ed il grafo target,
l'utente può eseguire l'algoritmo di matching. Al termine dell'esecuzione della
ricerca saranno mostrati il numero di occorrenze trovate e l'elenco dei match rilevati (si veda la \figurename~\ref{ricerca_netmatch}).

L'algoritmo di matching  si occupa anche di mostrare il numero totale di match trovati ed il numero di match distinti. 
Il motivo di questa distinzione sta nel fatto che, data una query di partenza, 
possono essere restituiti in output sottografi equivalenti (che rappresentano quindi, in
buona sostanza, la stessa struttura), ma che sono stati accoppiati in modo diverso.
Affinché due match risultino distinti, essi devono essere diversi in almeno un nodo.


\begin{figure}[!h]
\centering%
\linespread{1.0}
\subfigure[\label{opzioni_netmatch}]%
{\includegraphics[width=6cm]{figures/opzioni_netmatch.eps}}\qquad\qquad
\subfigure[\label{risultati_netmatch}]%
{\includegraphics[width=6cm]{figures/risultati_netmatch.eps}}
\caption[Esecuzione di una ricerca]{Esecuzione di una ricerca\label{ricerca_netmatch}.
(a) Selezione della query e del rete target su cui cercare tutte le occorrenze. L'utente può decidere
se nella ricerca si deve tenere conto anche delle etichette dei vertici e degli archi ({\ttfamily labeled}) e
se si deve tenere conto delle direzioni degli archi ({\ttfamily directed}). (b) Log e tabella
con i risultati: sono mostrati il numero di match totali e distinti trovati e nella tabella è fornito
l'elenco completo dei match distinti con indicatore del numero di match, numero di occorrenze per singolo
match e nodi coinvolti.}
\end{figure}

\section{Network Motifs}
Graphtool permette anche di disegnare alcune query predefinite tipicamente importanti in alcuni contesti come quello delle network biologiche \cite{milo}. 
È molto frequente, ad esempio, che le reti di trascrizione che regolano le risposte delle cellule
obbediscano ad alcuni principi che ricorrono più volte, identici, all'interno della stessa rete. Uno tra i modelli più
comuni e frequenti è certamente il cosiddetto feed-forward loop: esso è un modello a tre geni costituito da due fattori
di trascrizione di input (uno dei quali regola l'altro) ed un gene su cui effettuare la trascrizione. In
\figurename~\ref{feed-forward_loop} è mostrata una schematizzazione del \textit{feed-forward loop}
\index{Feed-forward loop} \cite{mangan}. Altri motifs predefiniti sono resi disponibili in Graphtool come ad esempio i motifs
\textit{Bi-Fan} (\figurename~\ref{bi-fan}) e \textit{Bi-Parallel} ({\figurename~\ref{bi-parallel}), spesso ricorrenti nell'ambito delle
reti neuronali e i motifs \textit{Fully connected triad} (\figurename~\ref{fully_connected_triad})e \textit{Uplinked mutual dyad} 
(\figurename~\ref{uplinked_mutual_dyad}), quest'ultimi di fondamentale rilevanza nel contesto del World Wide Web.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=6cm,keepaspectratio]{figures/feed-forward_loop.eps}
\caption[Feed-forward loop]{Feed-forward loop.}\label{feed-forward_loop}
\end{figure}

\begin{figure}[htbp]
\centering%
\linespread{1.0}
\subfigure[\label{bi-fan}]%
{\includegraphics[width=4cm]{figures/bi-fan.eps}}\qquad\qquad
\subfigure[\label{bi-parallel}]%
{\includegraphics[width=4cm]{figures/bi-parallel.eps}}\qquad\qquad
\subfigure[\label{fully_connected_triad}]%
{\includegraphics[width=4cm]{figures/fully_connected_triad.eps}}\qquad\qquad
\subfigure[\label{uplinked_mutual_dyad}]%
{\includegraphics[width=4cm]{figures/uplinked_mutual_dyad.eps}}
\caption[Motifs]{Motifs. (a) Bi-fan. (b) Bi-parallel. (c) Fully connected triad. (d) Uplinked mutual dyad.}
\end{figure}

\section{Motif Verification}\index{Motif Verification}
L'algoritmo di matching offre anche la possibilità di verificare se i match trovati in una network siano
statisticamente significativi (e non ottenuti per caso) e fornisce in tal senso una serie di indici 
calcolati opportunamente. In particolar modo, questa funzionalità permette di generare 
automaticamente una serie di network randomizzate che hanno caratteristiche simili al grafo 
target, sia in termini di numero di nodi
e di archi, che in termini di grado dei vertici. Di default la verifica viene effettuata su 100 network
randomizzate generate casualmente effettuando uno switch (scanbiando gli archi) sugli archi della network.
Per ciascuna delle network generate casualmente vengono calcolate le occorrenze del grafo query
calcolando \begin{math}E-value\end{math} e \begin{math}Z-score\end{math}:
\begin{equation}\index{E-value}
E-value = \frac{random newtork having occurrences\ge n_{real}}{number of random networks}
\end{equation}
\begin{equation}\index{Z-score}
Z-score = \frac{occurrences_{real}-occurrences_{random}}{sd_{rand}}
\end{equation}

\section{Un esempio completo}

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=12cm,keepaspectratio]{figures/query_netmatch.eps}
\caption[Query d'esempio]{Query d'esempio.}\label{query_netmatch}
\end{figure}

Mostriamo infine un esempio completo di esecuzione dell'algoritmo di matching in Graphtool. Si supponga
di voler trovare tutte le occorrenze del grafo mostrato in \figurename~\ref{query_netmatch} nel grafo
\texttt{galFiltered.sif}\footnote{Il grafo d'esempio rappresenta una rete d'interazione molecolare tra
331 geni.} mostrato in \figurename~\ref{target_netmatch}.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=12cm,keepaspectratio]{figures/target_netmatch.eps}
\caption[Target d'esempio]{Target d'esempio.}\label{target_netmatch}
\end{figure}

La ricerca, in questo esempio, viene effettuata senza tenere conto delle etichette dei nodi e degli archi.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=12cm,keepaspectratio]{figures/selezione_netmatch.eps}
\caption[Selezione di un match]{Selezione di un match.}\label{selezione_netmatch}
\end{figure}

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=12cm,keepaspectratio]{figures/motif_verification.eps}
\caption[Motif Verification]{Motif Verification.}\label{motif_verification}
\end{figure}

La query restituisce 22 match esatti (l'output è mostrato in \figurename~\ref{risultati_netmatch}). Al termine
dell'esecuzione della ricerca l'utente potrà selezionare un match in corrispondenza della tabella dei risultati e 
i nodi coinvolti saranno opportunamente evidenziati sul grafo target (\figurename~\ref{selezione_netmatch}).
L'utente può anche decidere di visualizzare il singolo match come nuovo grafo ed eventualmente
salvarlo.

%\begin{figure}[!h]
%\centering
%\linespread{1.0}
%\includegraphics[width=12cm,keepaspectratio]{figures/nuovo_grafo.eps}
%\caption[Visualizzazione come nuovo grafo di un match]{Visualizzazione come nuovo
%grafo di un match.}\label{nuovo_grafo}
%\end{figure}

Se si vuole, infine, eseguire la Motif Verification è sufficiente selezionare i valori switch/edges ed il numero di
network randomizzate che si vuole vengano generate: il risultato dettagliato sarà mostrato nel log ed in un'apposita finestra di
dialogo (\figurename~\ref{motif_verification}).
