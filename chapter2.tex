\chapter{Formati per la rappresentazione dei grafi}
Graphtool supporta svariati formati di file per la rappresentazione di grafi ed in particolare di reti biologiche e 
sociali. Tali formati sono compatibili con standard riconosciuti a livello internazionale. In questo capitolo 
verranno descritti in dettaglio i formati supportati con particolare attenzione allo standard Resource Description 
Framework (RDF) e delle notazioni usate per la rappresentazione dei dati.

\section{Simple Interaction Format}
Il formato Simple Interaction Format\index{Simple Interaction Format} (SIF) è un formato 
di rappresentazione di reti biologiche correntemente adottato da diversi tool tra cui Cytoscape, 
quest'ultima un'applicazione ampiamente usata nell'ambito della bioinformatica per la visualizzazione e l'analisi di reti biologiche 
\cite{shannon}.

\begin{figure}[htbp]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{node1 edge1 node2 node3\\
node2 edge2 node3\\
node3\\
node4 edge3 node4 node5}
\end{minipage}}}}
\caption[Grafo in formato SIF]{Grafo in formato SIF: il grafo rappresentato è costituito da 5 nodi e 3 vertici. 
Il nodo {\ttfamily node1} è connesso tramite l'arco {\ttfamily edge1} ai nodi {\ttfamily node2} e {\ttfamily node3}. 
Il nodo {\ttfamily node3} non ha archi in uscita, ma ha due archi in ingresso provenienti rispettivamente da 
{\ttfamily node1} e {\ttfamily node2}. Il nodo {\ttfamily node4}, inoltre, ha un arco avente se stesso come destinazione. 
Archi con indentica etichetta sono consentiti: saranno, tuttavia, caricati in memoria con id numerici differenti.}
\label{file_sif}
\end{figure}

Tale formato permette di rappresentare testualmente le interazione tra i vari nodi. Ogni riga assume il formato 
\texttt{source edge target1 ... targetN}. In \figurename~\ref{file_sif} è mostrato un esempio di grafo in formato 
SIF. In Cytoscape i tag per i nodi specificano l'id dell'arco (tipicamente una stringa di testo), mentre i tag 
degli archi specificano il tipo di relazione tra due o più nodi. In Graphtool i suddetti tag vengono 
interpreati come etichette (labels) dei nodi e degli archi essendo essi identificati mediante id numerici univoci. 
Il formato SIF consente, oltre che a definire le etichette dei nodi e degli archi, di associare ad essi anche 
attributi. Gli attributi sono definiti in due file separati aventi lo stesso nome del file SIF ma estensioni 
differenti: \texttt{NA} per il file degli attributi dei nodi e \texttt{EA} per il file degli attributi degli 
archi. Nelle figure \figurename~\ref{file_na} e \figurename~\ref{file_ea} sono mostrati due esempi di file degli 
attributi associabili al file in \figurename~\ref{file_sif}. Da notare che non è obbligatorio che ad un file in 
formato SIF siano associati i rispettivi file degli attributi.

\begin{figure}[htbp]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{node1 = nattr1\\
node2 = nattr2\\
node3 = nattr3\\
node4 = nattr4\\
node5 = nattr5}
\end{minipage}}}}
\caption[File NA con attributi dei nodi di un file SIF]{File NA con attributi dei nodi di un file SIF: ad ogni 
nodo è associato un solo attributo.}\label{file_na}
\end{figure}

\begin{figure}[htbp]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{node1 (edge1) node2 = eattr1\\
node1 (edge1) node3 = eattr2\\
node2 (edge2) node3 = eattr3\\
node4 (edge3) node4 = eattr4\\
node4 (edge3) node5 = eattr5}
\end{minipage}}}}
\caption[File EA con attributi degli archi di un file SIF]{File EA con attributi degli archi di un file SIF: 
per ogni arco con specifica etichetta, avente nodo sorgente e destinazione definiti, è associato un 
attributo.}\label{file_ea}
\end{figure}

\section{Graph Modelling Language}
Il formato Graph Modelling Language (GML)\index{Graph Modelling Language} è un formato portabile per la 
rappresentazione di grafi \cite{gml}. Le caratteristiche che lo contraddistinguono sono semplicità
d'interpretazione, estensibilità e particolare flessibilità. Un file GML consiste in una serie di coppie 
chiave-valore organizzate gerarchicamente. L'implementazione è molto flessibile poiché non è necessario che 
venga garantito un ordine specifico nella dichiarazione dei vari elementi che caratterizzano il grafo. Un 
grafo in formato GML è un insieme di liste di valori racchiuse tra parentesi quadre che identificano le 
caratteristiche principali della struttura. Ogni lista può contenere al suo interno ulteriori liste: l'idea 
di base è che un grafo è costituito da un insieme di nodi e di archi ed ogni nodo o arco può essere costituito 
da insiemi di proprietà (quali possono essere aspetto grafico o posizione). In \figurename~\ref{simple_gml} 
è mostrato un semplice grafo in formato GML che rappresenta un grafo costituito da un ciclo con 3 nodi.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{graph [\\
node [ id 1 label \textquotedbl{node1}\textquotedbl{} ]\\
node [ id 2 label \textquotedbl{node2}\textquotedbl{} ]\\
node [ id 3 label \textquotedbl{node3}\textquotedbl{} ]\\
edge [ source 1 target 2 label \textquotedbl{edge1}\textquotedbl{} ]\\
edge [ source 2 target 3 label \textquotedbl{}edge2\textquotedbl{} ]\\
edge [ source 2 target 3 label \textquotedbl{edge3}\textquotedbl{} ]\\
]}
\end{minipage}}}}
\caption[Grafo in formato GML]{Grafo in formato GML: dopo la definizione iniziale {\ttfamily graph} 
sono definiti tre nodi ({\ttfamily node}) per ognuno dei quali è definito un id, un etichetta e successivamente 
gli archi. Per ogni arco ({\ttfamily edge}) sono definiti, oltre che l'etichetta, i rispettivi id del nodo 
sorgente e del nodo destinazione.}\label{simple_gml}
\end{figure}

Possono, quindi, essere definiti per ciascun nodo od arco appositi parametri grafici in cui sono descritte 
caratteristiche relative al loro aspetto e le coordinate che identificano la posizione di ogni singolo nodo. 
In figura \figurename~\ref{graphics_gml} è riportato un esempio di grafo GML.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{graph [\\
node [ id 1 label \textquotedbl{node1}\textquotedbl{}\\
graphics [ x 1.0 y 1.0 w 2.0 h 2.0 color \textquotedbl{green}\textquotedbl{} ]\\
]\\
node [ id 2 label \textquotedbl{node2}\textquotedbl{}\\
graphics [ x 3.0 y 4.0 w 1.5 h 1.5 color \textquotedbl{red}\textquotedbl{} ]\\
]\\
edge [ source 1 target 2 label \textquotedbl{edge1}\textquotedbl{}\\
graphics [ color \textquotedbl{blue}\textquotedbl{} ]\\
]}\\
\end{minipage}}}}
\caption[Parametri grafici di un file GML]{Parametri grafici di un file GML: il parametro 
{\ttfamily graphics} definisce proprietà grafiche per i nodi e per gli archi. In questo caso 
{\ttfamily x} e {\ttfamily y} fissano le coordinate dei nodi mentre {\ttfamily w} e {\ttfamily h} 
larghezza ed altezza di nodi ed archi. Il parametro {\ttfamily color} definisce la colorazione del 
nodo o dell'arco.}\label{graphics_gml}
\end{figure}

\section{eXtensible Graph Markup and Modeling Language}
Il formato GML è particolarmente flessibile, ha una sintassi intuitiva ed è molto potente in quanto permette 
la rappresentazione di qualsiasi struttura dati arbitraria che va oltre quella dei grafi. Tuttavia esiste 
un'estensione del linguaggio di modellazione di grafi GML basata su XML, detta eXtensible Graph Markup and 
Modeling Language (XGMML)\index{XGMML}, che consente di rappresentare le medesime strutture dati sfruttando 
le potenzialità semantiche del diffuso metalinguaggio di markup incrementandone notevolmente l'espressività 
\cite{xgmml}. Un grafo espresso in formato GML può essere agevolmente convertito in XGMML e viceversa.
L'XML\index{XML} (eXtensible Markup Language) è un linguaggio di markup particolarmente flessibile ed 
essendo estendibile consente la definizione di tags personalizzati che possono essere utilizzati per i più 
svariati scopi: proprio per questo motivo le coppie chiavi-valori descritte per il formato GML possono essere
agevolmente espresse sotto forma di coppie attributi-valori di appositi tag XGMML. In \figurename~\ref{simple_xgmml} 
è mostrato il grafo visto in \figurename~\ref{simple_gml} espresso in notazione XGMML.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{<graph directed=\textquotedbl{1} id=\textquotedbl{1}\textquotedbl{} 
label=\textquotedbl{Graph}\textquotedbl{}>\\
<node id=\textquotedbl{1}\textquotedbl{} label=\textquotedbl{node 1}\textquotedbl{}> </node>\\
<node id=\textquotedbl{2}\textquotedbl{} label=\textquotedbl{node 2}\textquotedbl{}> </node>\\
<node id=\textquotedbl{3}\textquotedbl{} label=\textquotedbl{node 3}\textquotedbl{}> </node>\\
<edge source=\textquotedbl{1}\textquotedbl{} target=\textquotedbl{2}\textquotedbl{} 
label=\textquotedbl{edge 1}\textquotedbl{}> </edge>\\
<edge source=\textquotedbl{2}\textquotedbl{} target=\textquotedbl{3}\textquotedbl{} 
label=\textquotedbl{edge 2}\textquotedbl{}> </edge>\\
<edge source=\textquotedbl{3}\textquotedbl{} target=\textquotedbl{1}\textquotedbl{} 
label=\textquotedbl{edge 3}\textquotedbl{}> </edge>\\
</graph>}
\end{minipage}}}}
\caption[Grafo in formato XGMML]{Grafo in formato XGMML: il grafo è costituito da un ciclo con 3 nodi. 
Sono indicati anche un id ed un'etichetta associati al grafo. Il significato degli attributi dei tag 
relativi ai nodi ed agli archi è intuitivo.}\label{simple_xgmml}
\end{figure}

Analogamente al formato GML è possibile definire anche in XGMML una serie di tag che impostano le opzioni 
grafiche dei nodi e degli archi. Un esempio è mostrato in \figurename~\ref{graphics_xgmml}.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{<graph directed=\textquotedbl{1}\textquotedbl{} id=\textquotedbl{1}\textquotedbl{} 
label=\textquotedbl{Graph}\textquotedbl{}>\\
<node id=\textquotedbl{1}\textquotedbl{} label=\textquotedbl{node 1}\textquotedbl{}>\\
<graphics x=\textquotedbl{1.0}\textquotedbl{} y=\textquotedbl{1.0}\textquotedbl{} w =\textquotedbl{2.0}\textquotedbl{} 
h=\textquotedbl{2.0}\textquotedbl{} color=\textquotedbl{green}\textquotedbl{}> </graphics>\\
</node>\\
<node id=\textquotedbl{2}\textquotedbl{} label=\textquotedbl{node 2}\textquotedbl{}>\\
<graphics x=\textquotedbl{3.0}\textquotedbl{} y=\textquotedbl{4.0}\textquotedbl{} w =\textquotedbl{1.5}\textquotedbl{} 
h=\textquotedbl{1.5}\textquotedbl{} color=\textquotedbl{red}\textquotedbl{}> </graphics>\\
</node>\\
<edge source=\textquotedbl{1}\textquotedbl{} target=\textquotedbl{2}\textquotedbl{} 
label=\textquotedbl{edge 1}\textquotedbl{}>\\
<graphics color=\textquotedbl{blue}\textquotedbl{}> </graphics>\\
</edge>\\
</graph>}
\end{minipage}}}}
\caption[Parametri grafici di un grafo XGMML]{Parametri grafici di un grafo XGMML: il grafo è analogo a quello 
rappresentato in \figurename~\ref{graphics_gml} ed è costituito da due nodi interconnessi da un arco.}\label{graphics_xgmml}
\end{figure}

\section{Resource Description Framework}
Graphtool supporta pienamente il caricamento ed il salvataggio di grafi in formato Resource Descripion Framework 
(RDF)\index{Resource Description Framework}. RDF è uno standard proposto dal World Wide Web Consortium (W3C) 
\cite{rdf-model} nel 1999 ed usato come metodo generale per la descrizione concettuale e la modellazione di 
informazioni implementate sotto forma di risorse disponibili sul Web \cite{miller,rdf-primer}. Nella maggior parte 
dei casi è utilizzato per la rappresentazione di metadati sul Web, quali potrebbero essere titolo, autore o 
data di ultima modifica di una pagina web, ma, generalizzando, può essere applicato per la definizione di qualsiasi 
risorsa anche non disponibile nel web.

\subsection{Statements}
Lo standard RDF si basa sull'idea che le singole risorse possano essere identificate mediante Uniform Resource 
Identificator (URI). Un URI è una stringa di caratteri adibita all'identificazione univoca di una risorsa 
disponibile sul Web, ma anche di un file, un'immagine, un indirizzo di posta elettronica e quant'altro. 
Ogni concetto che può essere descritto con RDF è costituito da un insieme di proprietà che possono assumere 
determinati valori: la definizione di tali relazioni avviene mediante appositi \textit{statements}\index{Statement}, 
ossia triple di elementi chiamati rispettivamente \texttt{subject}\index{Subject}, \texttt{predicate}\index{Predicate} 
e \texttt{object}\index{Object}. Si veda \cite{rfc3986} per una descrizione dettagliata della sintassi URI.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=14cm,keepaspectratio]{figures/modello_dati_rdf.eps}
\caption[Modello dati RDF]{Modello dati RDF.}\label{modello_dati_rdf}
\end{figure}

Da questa premessa appare evidente che RDF consista nella definizione di una serie di triple di elementi 
che esprimono relazioni tra varie risorse. Risulta evidente, altresì, che tale notazione può essere 
agevolmente espressa sotto forma di grafo in cui ogni singola tripla rapresenta una coppia di nodi 
connessa con un arco. Particolare attenzione occorre prestare al fatto che i grafi RDF sono orientati: 
la direzione di ogni arco va sempre dal soggetto verso l'oggetto. 

Da quanto detto, durante lo sviluppo di Graphtool, si è voluto fare in modo che fosse in grado di elaborare 
dati RDF e di fornirne una rappresentazione grafica intuitiva. L'utente sarà in grado di analizzare tali 
dati ed eventualmente modificarli, garantendo comunque che in fase di salvataggio lo standard sia rispettato. 
In \figurename~\ref{simple_rdf_graph} è riportato un semplice grafo RDF costituito da due triple.

\begin{figure}[!h]
\centering
\linespread{1.0}
\includegraphics[width=14cm,keepaspectratio]{figures/simple_rdf_graph.eps}
\caption[Grafo RDF]{Grafo RDF: un semplice grafo RDF costuito da due triple aventi medesimo subject. In 
questo caso il grafo è costituito da tre nodi e due archi.}\label{simple_rdf_graph}
\end{figure}

Lo standard RDF impone vincoli ben precisi ai tipi di dati che i componenti di una singola tripla possono 
assumere \cite{rdf-concepts}. In particolare è consentito che il \texttt{subject} possa essere un URI o 
un blank node, il \texttt{predicate} esclusivamente un URI, mentre il \texttt{object} un URI, un blank 
node o un tipo literal. Mostriamo un dettaglio di queste queste caratteristiche.

\subsection{URIs}
Un URI\index{URI} consente di identificare una risorsa univoca (generalmente nel Web). Esso è una stringa 
di caratteri UNICODE non contenente alcun carattere di controllo ed identificata dalla presenza di un apposito 
separatore (\texttt{:}) che distingue lo schema (identificante il protocollo di 
comunicazione utilizzato) e una stringa (la cui interpretazione dipende dal tipo di schema) \cite{rfc3986}. 
La stringa può essere suddivisa in namespace e localname distinti mediante un apposito carattere di separazione.

\subsection{Literals}
Un literal\index{Literal} è una stringa di caratteri che può assumere due forme distinte: plain literal a cui può
essere associato un tag language opzionale \index{Language} oppure typed literal a cui è associato un tag 
datatype\index{Datatype} obbligatorio. Un datatype associato ad un typed literal consiste in un URI \cite{xml-datatypes-rdf}. 
Una descrizione dettagliata dei tag language si trova in \cite{rfc5646}.

\subsection{Blank nodes}
I blank nodes\index{Blank node} sono appartenenti ad un insieme infinito di elementi tale che esso, 
l'insieme infinito degli URI e l'insieme infinito dei literals siano a due a due disgiunti. In Graphtool 
un blank node è rappresentato come un nodo del grafo con etichetta uguale ad una stringa vuota di caratteri.

\section{Sintassi per il formato RDF}
Esistono svariate sintassi che permettono di esprimere grafi RDF: esse possono essere più o meno semplici 
notazioni testuali o serializzazioni in XML. In questo paragrafo vedremo le principali notazioni supportate 
da Graphtool sia in caricamento che in salvataggio.

\subsection{RDF/XML}
RDF/XML\index{RDF/XML} è la serializzazione più comune e più diffusa di grafi RDF. Il suo utilizzo è 
altamente raccomandato dal World Wide Web Consortium in quanto può essere utilizzato per codificare il 
modello di dati per lo scambio d'informazioni tra applicazioni \cite{rdf-concepts}. Tale serializzazione 
permette, altresì, la possibilità di poter effettuare uno scambio di informazioni tra RDF e qualsiasi altra applicazione XML. 
Un nodo RDF/XML può essere un URI, un literal o un blank node. Un nodo \texttt{subject} può essere un URI o un 
blank node: nel caso sia un URI, esso  rappresenta tipicamente una proprietà dell'elemento. Il \texttt{predicate} 
può essere esclusivamente un URI ed in questo caso rappresenta una relazione che intercorre tra soggetto ed oggetto. 
Un nodo \texttt{object}, invece, può anche essere un literal. Nel caso in cui il nodo è un blank node può essere 
associato opzionalmente ad esso un apposito tag detto blank node identifier. Tale identificatore viene usato per 
consentire di distinguere più agevolmente un blank node da un URI o da un literal. Graphtool permette di salvare 
in formato RDF/XML, come impostazione di default, impostando un blank node identifier per ciascun blank node del 
grafo. Un grafo RDF ed una sua possibile serializzazione in RDF/XML sono mostrati in \figurename~\ref{esempio_rdf}.

\begin{figure}[!h]
\centering%
\linespread{1.0}
\subfigure[\label{rdf_graph}]%
{\includegraphics[width=12cm]{figures/rdf_graph.eps}}\qquad\qquad
\subfigure[\label{rdfxml}]%
{{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{<rdf:RDF\\
xmlns:rdf=\textquotedbl{http://www.w3.org/1999/02/22-rdf-syntax-ns\#}\textquotedbl{}\\
xmlns:dc=\textquotedbl{http://purl.org/dc/elements/1.1/}\textquotedbl{}\\
xmlns:ex=\textquotedbl{http://www.example.org/}\textquotedbl{}>\\
\\
<rdf:Description rdf:about=\textquotedbl{http://www.example.org/example}\textquotedbl{}>\\
<ex:editor rdf:nodeID=\textquotedbl{A0}\textquotedbl{}/>\\
<dc:title>Example Page</dc:title>\\
</rdf:Description>\\
<rdf:Description rdf:nodeID=\textquotedbl{A0}\textquotedbl{}>\\
<ex:homepage rdf:resource=\textquotedbl{http://purl.org/net/mariorossi}\textquotedbl{}/>\\
<ex:name>Mario Rossi</ex:name>\\
</rdf:Description>\\
\\
</rdf:RDF>}
\end{minipage}}}}\label{rdfxml_graph}
}\qquad\qquad
\caption[Grafo RDF e sua serializzazione in RDF/XML]{Grafo RDF e sua serializzazione in 
RDF/XML\label{grafo_rdf}. (a) Il grafo RDF. (b) Una possibile serializzazione in RDF/XML: È differente 
la notazione per identificare gli URI dai literals nei nodi {\ttfamily subject} ed {\ttfamily object}. 
Gli archi sono URI per i quali è specificato un prefisso associato ad un localname. Le corrispondenze 
tra prefisso e specifico namespace sono elencate all'inizio del file: ad esempio al prefisso {\ttfamily ex} 
è associato il namespace {\ttfamily http://www.example.org}. Il blank node è identificato dall'id 
{\ttfamily A0} ed è presente in due distinte triple: infatti gli archi uscenti da esso nel grafo sono 
due.}\label{esempio_rdf}
\end{figure}

I nodi plain literal possono avere un attributo language opzionale mentre i nodi typed literal hanno un 
attributo datatype obbligatorio. Nel primo caso nodi con la stessa etichetta, ma con attributi language, 
devono essere rappresentati da Graphtool come vertici distinti. In \figurename~\ref{language} è mostrato 
un semplice grafo RDF in cui sono definiti alcuni tags language.

\begin{figure}[!h]
\centering%
\linespread{1.0}
\subfigure[\label{language1}]%
{\includegraphics[width=12cm]{figures/language.eps}}\qquad\qquad
\subfigure[\label{language2}]%
{{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{<rdf:RDF\\
xmlns:rdf=\textquotedbl{http://www.w3.org/1999/02/22-rdf-syntax-ns\#}\textquotedbl{}\\
xmlns:ex=\textquotedbl{http://www.example.org/}\textquotedbl{}>\\
\\
<rdf:Description rdf:about=\textquotedbl{http://www.example.org/example}\textquotedbl{}>\\
<ex:name>Example Page</ex:name>\\
<ex:name xml:lang=\textquotedbl{it}\textquotedbl{}>Example Page</ex:name>\\
<ex:name xml:lang=\textquotedbl{en}\textquotedbl{}>Example Page</ex:name>\\
</rdf:Description>\\
\\
</rdf:RDF>}
\end{minipage}}}}
}\qquad\qquad
\caption[Grafo RDF con language tags]{Grafo RDF con language tags. (a) Il grafo RDF. (b) 
Serializzazione in RDF/XML.}\label{language}
\end{figure}

Le specifiche complete della sintassi RDF/XML sono riportate in \cite{rdf-syntax}. Una successiva 
revisione del formato RDF/XML detta RDF Revised, che risolve alcuni problemi sintattici intrinsechi 
ad RDF/XML, è descritta in \cite{beckett-rdf}.

\subsection{N-Triples}
La notazione N-Triples\index{N-Triples}, tipicamente usata a fini di testing, è una semplice 
notazione testuale per esprimere in modalità chiara ed intuitiva le triple di un documento RDF 
\cite{rdf-ntriples}. Un documento N-Triples è una sequenza di dichiarazioni \texttt{subject predicate 
object}, i cui delimitatori differiscono nel caso in cui siano URI, blank nodes o literals. In 
\figurename~\ref{grafo_ntriples} è mostrato il grafo visto in \figurename~\ref{rdfxml_graph} 
espresso nella notazione N-Triples.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\footnotesize
\texttt{\_:a <http://www.example.org/homepage> <http://purl.org/net/mariorossi/> .\\
\_:a <http://www.example.org/name> \textquotedbl{Mario Rossi}\textquotedbl{} .\\
<http://www.example.org/example> <http://www.example.org/example> \_:a .\\
<http://www.example.org/example> <http://purl.org/dc/elements/1.1/title> \textquotedbl{Example Page}\textquotedbl{} .}
\end{minipage}}}}
\caption[Grafo in formato N-Triples]{Grafo in formato N-Triples.}\label{grafo_ntriples}
\end{figure}

\subsection{Notation 3}
Notation 3 (N3)\index{Notation 3} è un'ulteriore notazione usata per esprimere grafi in formato 
RDF. Ha un formato compatto e leggibile che costituisce una valida alternativa alla serializzazione 
in XML conferendone, altresì, maggiore espressività e simmetricità \cite{rdf-n3}. Tale notazione è 
particolarmente adatta per fornire un elenco chiaro e leggibile delle triple RDF. Analogamente alla 
notazione N-Triples è possibile specificare le singole triple riga per riga apponendo appositi 
delimitatori a seconda che gli elementi siano URI, blank nodes o literal. In \figurename~\ref{simple-n3} 
è mostrato un semplice file N3 con una tripla RDF.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{<http://www.example.org/example> <http://www.example.org/name> \textquotedbl{Mario Rossi}\textquotedbl{};}
\end{minipage}}}}
\caption[Semplice grafo in formato Notation 3]{Semplice grafo in formato Notation 3: la notazione 
è molto simile alla N-Triples.}\label{simple-n3}
\end{figure}

A differenza della notazione N-Triples, tuttavia, è possibile semplificare la sintassi nel caso in 
cui più triple abbiano lo stesso \texttt{subject}. Come nella sintassi RDF/XML, è possibile 
specificare appositi \texttt{prefix} da associare ai namespaces dei riferimenti URI \cite{n3-primer}. 
In \figurename~\ref{n3-example} è fornito un esempio completo di rappresentazione di un grafo in Notation 3.
Esiste anche una notazione più semplice della Notation3, chiamata Turtle \cite{beckett-turtle}, che ne costituisce un sottoinsieme:
anch'essa è pienamente supportata in lettura e scrittura da Graphtool.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{@prefix p: <http://www.example.org/personal\_details\#> .\\
@prefix m: <http://www.example.org/meeting\_organization\#> .\\\\
<http://www.example.org/people\#fred>\\
p:GivenName \textquotedbl{Fred}\textquotedbl{};\\
p:hasEmail <mailto:fred@example.com>;\\
m:attending <http://meetings.example.com/cal\#m1> .\\\\
<http://meetings.example.com/cal\#m1>\\
m:homePage <http://meetings.example.com/m1/hp> .}
\end{minipage}}}}
\caption[Grafo in formato Notation 3]{Grafo in formato Notation 3: in questo caso al namespace 
{\ttfamily http://www.example.org/meeting\_organization\#} è associato il prefisso {\ttfamily m} 
e al namespace {\ttfamily http://www.example.org/personal\_details\#} il prefisso {\ttfamily p}. 
Il nodo {\ttfamily http://www.example.org/people\#fred} ha tre archi in uscita definiti nelle tre 
righe successive. Il nodo {\ttfamily http://meetings.example.com/cal\#m1} ha, invece, un solo arco 
in uscita. La rappresentazione di questo grafo in notazione Turtle è, in questo caso, identica.}\label{n3-example}
\end{figure}

\subsection{TriX}

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{<TriX>\\
<graph>\\
<triple>\\
<id>x</id>\\
<uri>http://example.org/wife</uri>\\
<uri>http://example.org/Mary</uri>\\
</triple>\\
<triple>\\
<uri>http://example.org/Bob</uri>\\
<uri>http://example.org/name</uri>\\
<plainLiteral>Bob</plainLiteral>\\
</triple>\\
<triple>\\
<uri>http://example.org/Mary</uri>\\
<uri>http://example.org/age</uri>\\
<typedLiteral\\datatype=\textquotedbl{http://www.w3.org/2001/XMLSchema\#integer}\textquotedbl{}\\>32</typedLiteral>\\
</triple>\\
</graph>\\
</TriX>}
\end{minipage}}}}
\caption[Grafo in formato TriX]{Grafo in formato TriX: si distinguono chiaramente, per ogni tripla, 
gli URI, i blank nodes, i plain literals ed i typed literals. Il tag {\ttfamily id} identifica un 
blank node.}\label{grafo_trix}
\end{figure}

Il formato RDF/XML presenta diversi problemi che non ne possono consentire un uso completo, soprattutto 
per quanto riguarda problemi relativi alla validazione e per quanto riguarda l'interoperabilità con 
altre applicazioni XML. Per questo motivo si è tentato d'introdurre una nuova notazione, sintatticamente 
più semplice e pur sempre basata su XML: essa prende il nome di TriX\index{TriX} \cite{trix2}. 
Un documento in formato TriX è un documento XML il cui grafo è delimitato da tag \texttt{graph} ed ogni 
tripla è delimitata da appositi tag \texttt{triple}. Sono esplicitamente distinti tag che identificano 
blank node, URI, plain literal e typed literal. Un esempio è mostrato in \figurename~\ref{grafo_trix}.
Graphtool, oltre ai formati RDF già descritti, supporta, anche in questo caso, caricamento e salvataggio 
di questo formato. 

\subsection{TriG}

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns\#> .\\
@prefix ex: <http://www.example.org/vocabulary\#> .\\
@prefix : <http://www.example.org/exampleDocument\#> .\\
\\
:G1 \{ :Mario ex:name "Mario Rossi" . \\     
      :Mario ex:homepage <http://www.mariorossi.it> .\\
      :Mario ex:email <mailto:mario@mariorossi.it> .\\
      :Mario ex:hasSkill ex:Management \}\\
\\
:G2 \{ :Mario rdf:type ex:Person .\\
      :Mario ex:hasSkill ex:Programming \}\\
}
\end{minipage}}}}
\caption[Grafo in formato TriG]{Grafo in formato TriG: in questo file sono descritti due differenti grafi (G1 e G2)
separati dai delimitatori \texttt{\{} e \texttt{\}}. La definizione delle triple e dei namespaces è identica a 
Turtle.}\label{grafo_trig}
\end{figure}

Il formato TriG è una sintassi per descrivere grafi RDF che nasce come valida alternativa a TriX ed ha una sintassi
simile a Turtle, di cui ne costituisce un'estensione \cite{trig}. Tale sintassi aggiunge due nuovi delimitatori
per permettere di raggruppare più grafi in un unico file e permette di associare ad ogni grafo un nome.
In \figurename~\ref{grafo_trig} è mostrato un semplice esempio in cui sono descritti due differenti grafi.

